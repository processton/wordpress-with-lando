<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'database_name_here' );

/** MySQL database username */
define( 'DB_USER', 'username_here' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password_here' );

/** MySQL hostname */
define( 'DB_HOST', 'database_host_here' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'k8eLLLQd!h6X,*C/_X[H72I}pwL|+HE3;s(x <|}iam76_&dGw~>ggpyW?J7^#j9' );
define( 'SECURE_AUTH_KEY',  '2wg0:5|RFtSO:0}1|W FYBnzG9$/*vhtd8CQR*P;}*AA[L@B@K@OE>obeUS7!;*C' );
define( 'LOGGED_IN_KEY',    '#d`F)C^?9Y%pq^}QGqq?j(Hn$q7rW@O2gx7J,4$:CG=sM|r{/X]b|5N^A3[tGu#9' );
define( 'NONCE_KEY',        'W;~O$sDFpcQ:s9lXO,c^M8<f`q{70@7ys/#E9-8N881onnOcR PT;dLGzX;QsMHq' );
define( 'AUTH_SALT',        'qXn^)`(JY,L`8Q)4EhLf~foiFI3s~kgJbA7RxnIqUZ3q/yxh-}YP~Ds=e4]pj?X@' );
define( 'SECURE_AUTH_SALT', 'j1@Es0H+?T_# MzEO?6ozTCs{7?),w _(`9,(7;;:.=W|y-6)0XY3viRga,sM,zC' );
define( 'LOGGED_IN_SALT',   '7%P]ESuuNkQ(yf5VI_?7ht]=[Qu[OhP|C<3svc~dVm5!yQ5zu(Oy|DT[Uf#t`HC#' );
define( 'NONCE_SALT',       '.l%|/7v^l&^rl<g-%V$Z>a>-UiKKf73=&T+f?g:N~(]Yo.DyY0&k+4^tKUFs!#1U' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
